import javax.swing.*;

public class Foothill {

	 public static void main(String[] args) {
	      // establish main frame in which program will run
	      TypingFrame myTypingFrame = new TypingFrame("Copy The String You See ...");
	      myTypingFrame.setSize(400, 200);
	      myTypingFrame.setLocationRelativeTo(null);
	      myTypingFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	      
	      // show everything to the user
	      myTypingFrame.setVisible(true);
	 }
	
}
