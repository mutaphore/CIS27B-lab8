import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Date;

public class TypingFrame extends JFrame {

	private JButton btnMyButton;
   private JTextField txtMyTextArea;
   private JLabel lblMyLabel;
   private JLabel lblJudge;
   private String current_string;
   private int current_index;
   private String[] strings = {"i thought about you",
         							 "spring can really hang you up the most",
         							 "night in tunesia",
         							 "freedom jazz dance",
   									 "haitian divorce",
   									 "National park camping",
   									 "You are very beautiful"};
   private JLabel lblTimer;
   TimeKeeperTask timekeeper;
   private long Start_Date; //keeps track of start time
	private long Time_Elapsed; //Time difference between start and finish in milliseconds
 
   // TypingFrame constructor
   public TypingFrame(String title)
   {
      // pass the title up to the JFrame constructor
      super(title);
            
      current_index = 0;
      current_string = strings[current_index];

      // set up layout which will control placement of buttons, etc.
      FlowLayout layout = new FlowLayout(FlowLayout.CENTER, 200, 10);  
      setLayout(layout);
           
      // 4 controls: two labels, a text field and a button
      lblMyLabel = new JLabel(current_string);
      lblTimer = new JLabel("       ");
      lblJudge = new JLabel("       ");
      SetTimeLabel();
      txtMyTextArea = new JTextField(30);
      btnMyButton = new JButton("Press For New String"); 
      add(lblMyLabel);
      add(lblJudge);
      add(txtMyTextArea);
      add(btnMyButton);
      add(lblTimer);
      
      lblJudge.setVisible(false); //Hide judge label for now until checkpoint      
      btnMyButton.addActionListener( new ButtonListener() );
      txtMyTextArea.addActionListener( new TextFieldListener() );
      timekeeper = new TimeKeeperTask(this);
      StartClock();
      
      // Save start time to calculate time elapsed later
      Start_Date = new Date().getTime();
      
   }
   
   // inner class for JButton Listener - select new string
   class ButtonListener implements ActionListener
   {
      // event handler for JButton
      public void actionPerformed(ActionEvent e)
      {
         int new_index;
         lblJudge.setVisible(false);
         do
           new_index = (int)(Math.random() * 1000) % strings.length ;
         while (new_index == current_index);
         current_index = new_index;
         current_string = strings[new_index];
         lblMyLabel.setText(current_string);
         txtMyTextArea.setText("");
         txtMyTextArea.requestFocus();
         
         // start a thread to make clock tick
         StartClock();        
         
         // Save start time to calculate time elapsed later
         Start_Date = new Date().getTime();
      }
   }// end inner class ButtonListener 
   
   // inner class for JTextField Listener
   class TextFieldListener implements ActionListener
   {
      // event handler for JTextField
      public void actionPerformed(ActionEvent e)
      {         
         String strUser = txtMyTextArea.getText();
                  
         if (current_string.equals(strUser))
            lblJudge.setText("<Correct!>");
         else
            lblJudge.setText("<Sorry, the string entered is incorrect.>");  
         
         //Display message to lblJudge
         lblJudge.setVisible(true);
         
         // stop the thread 
         StopClock();         
       }
   } // end inner class TextFieldListener
   
   //Set timer label to time elapsed in MM:SS format
   void SetTimeLabel()
   {
   	Time_Elapsed = new Date().getTime() - Start_Date;
      lblTimer.setText(TimeFormatter(Time_Elapsed/1000));
   }
   
   //Set timer label to seconds elapsed
   void SetTimeLabelSeconds()
   {
      lblTimer.setText("Seconds elapsed: " + Long.toString(Time_Elapsed/1000));
   }
   
   void StartClock()
   {
      // if an existing thread is running, stop it
      StopClock();
      Thread thread = new Thread(timekeeper);
      timekeeper.TurnMeOn();
      thread.start();     
   }
   
   void StopClock()
   {
      timekeeper.TurnMeOff();
   }
   
   //Formats time passed in seconds into MM:SS format
   private String TimeFormatter(long Time_Elapsed) {
   	
   	StringBuffer temp = new StringBuffer();
   	long minutes;
   	long seconds;
   	String minute_digit = "";
   	String second_digit = "";
   	
   	minutes = Time_Elapsed / 60;
   	seconds = Time_Elapsed % 60;
   	
   	if(minutes < 10)
   		minute_digit = "0";
   	if(seconds < 10)
   		second_digit = "0";
   	
   	temp.append(minute_digit + minutes + ":" + second_digit + seconds);
   	
   	return temp.toString();
   	
   }
	
}
