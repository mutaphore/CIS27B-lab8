
public class TimeKeeperTask implements Runnable{
	
	TypingFrame parent;
   boolean active;
   final int SLEEPTIME = 200; // .2 second
   
   TimeKeeperTask(TypingFrame pnt)
   {
      parent = pnt;
      active = true;
   }
   
   public void TurnMeOff()
   {
      active = false;
      try
      {
         Thread.sleep(SLEEPTIME+1); // give it time to get the message
      }
      catch (InterruptedException ex)
      {          
      }
   }
   
   public void TurnMeOn()
   {
      active = true;
   }
  
   public void run()
   {
      while (active)
      {
         try
         {
            Thread.sleep(SLEEPTIME);
         }
         catch (InterruptedException ex)
         {          
         }
         parent.SetTimeLabel(); 
      }
      
      parent.SetTimeLabelSeconds(); 
      
   }
   
}
